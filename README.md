# WAB_cp_workshop_may2023

Thanks again to everyone who joined the workshop on May 11th during the Wolves Across Borders Conference in Stockholm 2023.

I have uploaded the answers to the assignment and the presentation about changepoint analysis. 


If there are any questions feel free to contact me at erik.versluijs@inn.no


